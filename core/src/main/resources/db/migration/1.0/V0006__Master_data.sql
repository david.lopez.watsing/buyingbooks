ALTER TABLE Book ADD FOREIGN KEY (idAuthor) REFERENCES Author (id);
ALTER TABLE Purchase ADD FOREIGN KEY (idClient) REFERENCES Client (id);
ALTER TABLE Purchase ADD FOREIGN KEY (idBook) REFERENCES Book (id);
INSERT INTO Author
(
   modificationCounter,
   name,
   lastname
)
VALUES
(
   1,
   'David',
   'Lopez'
);
INSERT INTO Author
(
   modificationCounter,
   name,
   lastname
)
VALUES
(
   1,
   'Jose',
   'Perez'
);
INSERT INTO Author
(
   modificationCounter,
   name,
   lastname
)
VALUES
(
   1,
   'Juan',
   'Tamariz'
);
INSERT INTO Author
(
   modificationCounter,
   name,
   lastname
)
VALUES
(
   1,
   'Pascual',
   'Lopez'
);
INSERT INTO Author
(
   modificationCounter,
   name,
   lastname
)
VALUES
(
   1,
   'George',
   'R.R. Martin'
);
INSERT INTO Client
(
   modificationCounter,
   name,
   lastname
)
VALUES
(
   1,
   'David',
   'Lopez'
);
INSERT INTO Client
(
   modificationCounter,
   name,
   lastname
)
VALUES
(
   1,
   'Pepe',
   'Luis'
);
INSERT INTO Book
(
   modificationCounter,
   name,
   stock,
   idAuthor
)
VALUES
(
   1,
   'Juego de Tronos',
   100,
   5
);
INSERT INTO Book
(
   modificationCounter,
   name,
   stock,
   idAuthor
)
VALUES
(
   1,
   'Choque de Reyes',
   100,
   5
);
INSERT INTO Book
(
   modificationCounter,
   name,
   stock,
   idAuthor
)
VALUES
(
   1,
   'Tormenta de Espadas',
   100,
   5
);
INSERT INTO Book
(
   modificationCounter,
   name,
   stock,
   idAuthor
)
VALUES
(
   1,
   'Danza de Dragones',
   100,
   5
);
INSERT INTO Book
(
   modificationCounter,
   name,
   stock,
   idAuthor
)
VALUES
(
   1,
   'Festin de Cuervos',
   100,
   5
);
INSERT INTO Purchase
(
   modificationCounter,
   idClient,
   idBook,
   purchaseDate
)
VALUES
(
   1,
   1,
   1,
   curdate ()
);
INSERT INTO Purchase
(
   modificationCounter,
   idClient,
   idBook,
   purchaseDate
)
VALUES
(
   1,
   2,
   2,
   curdate ()
);