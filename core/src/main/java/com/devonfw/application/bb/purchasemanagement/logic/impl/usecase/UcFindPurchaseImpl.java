package com.devonfw.application.bb.purchasemanagement.logic.impl.usecase;

import java.util.Optional;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.bb.purchasemanagement.dataaccess.api.PurchaseEntity;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseEto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseSearchCriteriaTo;
import com.devonfw.application.bb.purchasemanagement.logic.api.usecase.UcFindPurchase;
import com.devonfw.application.bb.purchasemanagement.logic.base.usecase.AbstractPurchaseUc;

/**
 * Use case implementation for searching, filtering and getting Purchases
 */
@Named
@Validated
@Transactional
public class UcFindPurchaseImpl extends AbstractPurchaseUc implements UcFindPurchase {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindPurchaseImpl.class);

  @Override
  public PurchaseEto findPurchase(long id) {

    LOG.debug("Get Purchase with id {} from database.", id);
    Optional<PurchaseEntity> foundEntity = getPurchaseRepository().findById(id);
    if (foundEntity.isPresent())
      return getBeanMapper().map(foundEntity.get(), PurchaseEto.class);
    else
      return null;
  }

  @Override
  public Page<PurchaseEto> findPurchases(PurchaseSearchCriteriaTo criteria) {

    Page<PurchaseEntity> purchases = getPurchaseRepository().findByCriteria(criteria);
    return mapPaginatedEntityList(purchases, PurchaseEto.class);
  }

}
