package com.devonfw.application.bb.bookmanagement.logic.impl.usecase;

import java.util.List;
import java.util.Optional;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.bb.bookmanagement.dataaccess.api.BookEntity;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookEto;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookSearchCriteriaTo;
import com.devonfw.application.bb.bookmanagement.logic.api.usecase.UcFindBook;
import com.devonfw.application.bb.bookmanagement.logic.base.usecase.AbstractBookUc;

/**
 * Use case implementation for searching, filtering and getting Books
 */
@Named
@Validated
@Transactional
public class UcFindBookImpl extends AbstractBookUc implements UcFindBook {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindBookImpl.class);

  @Override
  public BookEto findBook(long id) {

    LOG.debug("Get Book with id {} from database.", id);
    Optional<BookEntity> foundEntity = getBookRepository().findById(id);
    if (foundEntity.isPresent())
      return getBeanMapper().map(foundEntity.get(), BookEto.class);
    else
      return null;
  }

  @Override
  public Page<BookEto> findBooks(BookSearchCriteriaTo criteria) {

    Page<BookEntity> books = getBookRepository().findByCriteria(criteria);
    return mapPaginatedEntityList(books, BookEto.class);
  }

  @Override
  public List<BookEto> getBooksByClient(long id) {

    List<BookEntity> books = getBookRepository().getBooksByClient(id);
    return getBeanMapper().mapList(books, BookEto.class);
  }

}
