package com.devonfw.application.bb.authormanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.devonfw.application.bb.authormanagement.dataaccess.api.AuthorEntity;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link AuthorEntity}
 */
public interface AuthorRepository extends DefaultRepository<AuthorEntity> {

  /**
   * @param criteria the {@link AuthorSearchCriteriaTo} with the criteria to search.
   * @return the {@link Page} of the {@link AuthorEntity} objects that matched the search. If no pageable is set, it
   *         will return a unique page with all the objects that matched the search.
   */
  default Page<AuthorEntity> findByCriteria(AuthorSearchCriteriaTo criteria) {

    AuthorEntity alias = newDslAlias();
    JPAQuery<AuthorEntity> query = newDslQuery(alias);

    String name = criteria.getName();
    if (name != null && !name.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getName()), name, criteria.getNameOption());
    }
    String lastname = criteria.getLastname();
    if (lastname != null && !lastname.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getLastname()), lastname, criteria.getLastnameOption());
    }
    if (criteria.getPageable() == null) {
      criteria.setPageable(PageRequest.of(0, Integer.MAX_VALUE));
    } else {
      addOrderBy(query, alias, criteria.getPageable().getSort());
    }

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   * 
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<AuthorEntity> query, AuthorEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "name":
            if (next.isAscending()) {
              query.orderBy($(alias.getName()).asc());
            } else {
              query.orderBy($(alias.getName()).desc());
            }
            break;
          case "lastname":
            if (next.isAscending()) {
              query.orderBy($(alias.getLastname()).asc());
            } else {
              query.orderBy($(alias.getLastname()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}