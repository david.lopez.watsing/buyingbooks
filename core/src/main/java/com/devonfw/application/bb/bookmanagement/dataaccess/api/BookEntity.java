package com.devonfw.application.bb.bookmanagement.dataaccess.api;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.devonfw.application.bb.authormanagement.dataaccess.api.AuthorEntity;
import com.devonfw.application.bb.bookmanagement.common.api.Book;
import com.devonfw.application.bb.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author dlopezwa
 */
@Entity
@Table(name = "Book")
public class BookEntity extends ApplicationPersistenceEntity implements Book {

  private static final long serialVersionUID = 1L;

  @NotNull
  @NotEmpty
  private String name;

  @NotNull
  private int stock;

  private AuthorEntity authorEntity;

  /**
   * The constructor.
   */
  public BookEntity() {

    super();
  }

  /**
   * The constructor.
   *
   * @param name
   * @param stock
   * @param authorEntity
   */
  public BookEntity(@NotNull @NotEmpty String name, @NotNull int stock, AuthorEntity authorEntity) {

    super();
    this.name = name;
    this.stock = stock;
    this.authorEntity = authorEntity;
  }

  /**
   * @return name
   */
  @Override
  public String getName() {

    return this.name;
  }

  /**
   * @param name new value of {@link #getname}.
   */
  @Override
  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return stock
   */
  @Override
  public int getStock() {

    return this.stock;
  }

  /**
   * @param stock new value of {@link #getstock}.
   */
  @Override
  public void setStock(int stock) {

    this.stock = stock;
  }

  /**
   * @return authorEntity
   */
  @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
  @JoinColumn(name = "idAuthor")
  public AuthorEntity getAuthorEntity() {

    return this.authorEntity;
  }

  /**
   * @param authorEntity new value of {@link #getauthorEntity}.
   */
  public void setAuthorEntity(AuthorEntity authorEntity) {

    this.authorEntity = authorEntity;
  }

  @Override
  @Transient
  public Long getAuthorEntityId() {

    if (this.authorEntity == null) {
      return null;
    }
    return this.authorEntity.getId();
  }

  @Override
  public void setAuthorEntityId(Long authorEntityId) {

    if (authorEntityId == null) {
      this.authorEntity = null;
    } else {
      AuthorEntity authorEntity = new AuthorEntity();
      authorEntity.setId(authorEntityId);
      this.authorEntity = authorEntity;
    }
  }

}
