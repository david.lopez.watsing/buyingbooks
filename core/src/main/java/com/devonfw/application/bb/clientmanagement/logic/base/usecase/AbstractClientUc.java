package com.devonfw.application.bb.clientmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.devonfw.application.bb.clientmanagement.dataaccess.api.repo.ClientRepository;
import com.devonfw.application.bb.general.logic.base.AbstractUc;

/**
 * Abstract use case for Clients, which provides access to the commonly necessary data access objects.
 */
public class AbstractClientUc extends AbstractUc {

  /** @see #getClientRepository() */
  @Inject
  private ClientRepository clientRepository;

  /**
   * Returns the field 'clientRepository'.
   * 
   * @return the {@link ClientRepository} instance.
   */
  public ClientRepository getClientRepository() {

    return this.clientRepository;
  }

}
