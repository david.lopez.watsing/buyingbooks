package com.devonfw.application.bb.purchasemanagement.dataaccess.api;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.devonfw.application.bb.bookmanagement.dataaccess.api.BookEntity;
import com.devonfw.application.bb.clientmanagement.dataaccess.api.ClientEntity;
import com.devonfw.application.bb.general.dataaccess.api.ApplicationPersistenceEntity;
import com.devonfw.application.bb.purchasemanagement.common.api.Purchase;

/**
 * @author dlopezwa
 */
@Entity
@Table(name = "Purchase")
public class PurchaseEntity extends ApplicationPersistenceEntity implements Purchase {

  private ClientEntity clientEntity;

  private BookEntity book;

  @Temporal(TemporalType.TIMESTAMP)
  private Timestamp purchaseDate;

  private static final long serialVersionUID = 1L;

  /**
   * The constructor.
   */
  public PurchaseEntity() {

    super();
  }

  /**
   * The constructor.
   *
   * @param client
   * @param book
   * @param purchaseDate
   */
  public PurchaseEntity(ClientEntity client, BookEntity book, Timestamp purchaseDate) {

    super();

    this.clientEntity = client;
    this.book = book;
    this.purchaseDate = purchaseDate;
  }

  /**
   * @return client
   */
  @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
  @JoinColumn(name = "idClient")
  public ClientEntity getClientEntity() {

    return this.clientEntity;
  }

  /**
   * @param client new value of {@link #getclient}.
   */

  public void setClientEntity(ClientEntity clientEntity) {

    this.clientEntity = clientEntity;
  }

  /**
   * @return book
   */
  @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
  @JoinColumn(name = "idBook")
  public BookEntity getBook() {

    return this.book;
  }

  /**
   * @param book new value of {@link #getbook}.
   */
  public void setBook(BookEntity book) {

    this.book = book;
  }

  /**
   * @return purchaseDate
   */
  @Override
  public Timestamp getPurchaseDate() {

    return this.purchaseDate;
  }

  /**
   * @param purchaseDate new value of {@link #getpurchaseDate}.
   */
  @Override
  public void setPurchaseDate(Timestamp purchaseDate) {

    this.purchaseDate = purchaseDate;
  }

  @Override
  @Transient
  public Long getClientId() {

    if (this.clientEntity == null) {
      return null;
    }
    return this.clientEntity.getId();
  }

  @Override
  public void setClientId(Long clientId) {

    if (clientId == null) {
      this.clientEntity = null;
    } else {
      ClientEntity clientEntity = new ClientEntity();
      clientEntity.setId(clientId);
      this.clientEntity = clientEntity;
    }
  }

  @Override
  @Transient
  public Long getBookId() {

    if (this.book == null) {
      return null;
    }
    return this.book.getId();
  }

  @Override
  public void setBookId(Long bookId) {

    if (bookId == null) {
      this.book = null;
    } else {
      BookEntity bookEntity = new BookEntity();
      bookEntity.setId(bookId);
      this.book = bookEntity;
    }
  }

}
