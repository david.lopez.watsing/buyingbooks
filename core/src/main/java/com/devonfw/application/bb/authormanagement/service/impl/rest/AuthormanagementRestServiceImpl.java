package com.devonfw.application.bb.authormanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.authormanagement.logic.api.Authormanagement;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorEto;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorSearchCriteriaTo;
import com.devonfw.application.bb.authormanagement.service.api.rest.AuthormanagementRestService;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Authormanagement}.
 */
@Named("AuthormanagementRestService")
public class AuthormanagementRestServiceImpl implements AuthormanagementRestService {

  @Inject
  private Authormanagement authormanagement;

  @Override
  public AuthorEto getAuthor(long id) {

    return this.authormanagement.findAuthor(id);
  }

  @Override
  public AuthorEto saveAuthor(AuthorEto author) {

    return this.authormanagement.saveAuthor(author);
  }

  @Override
  public void deleteAuthor(long id) {

    this.authormanagement.deleteAuthor(id);
  }

  @Override
  public Page<AuthorEto> findAuthors(AuthorSearchCriteriaTo searchCriteriaTo) {

    return this.authormanagement.findAuthors(searchCriteriaTo);
  }
}