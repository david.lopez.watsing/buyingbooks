package com.devonfw.application.bb.clientmanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.clientmanagement.logic.api.Clientmanagement;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientEto;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientSearchCriteriaTo;
import com.devonfw.application.bb.clientmanagement.service.api.rest.ClientmanagementRestService;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Clientmanagement}.
 */
@Named("ClientmanagementRestService")
public class ClientmanagementRestServiceImpl implements ClientmanagementRestService {

  @Inject
  private Clientmanagement clientmanagement;

  @Override
  public ClientEto getClient(long id) {

    return this.clientmanagement.findClient(id);
  }

  @Override
  public ClientEto saveClient(ClientEto client) {

    return this.clientmanagement.saveClient(client);
  }

  @Override
  public void deleteClient(long id) {

    this.clientmanagement.deleteClient(id);
  }

  @Override
  public Page<ClientEto> findClients(ClientSearchCriteriaTo searchCriteriaTo) {

    return this.clientmanagement.findClients(searchCriteriaTo);
  }
}