package com.devonfw.application.bb.purchasemanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.purchasemanagement.logic.api.Purchasemanagement;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseEto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseInCto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseSearchCriteriaTo;
import com.devonfw.application.bb.purchasemanagement.service.api.rest.PurchasemanagementRestService;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Purchasemanagement}.
 */
@Named("PurchasemanagementRestService")
public class PurchasemanagementRestServiceImpl implements PurchasemanagementRestService {

  @Inject
  private Purchasemanagement purchasemanagement;

  @Override
  public PurchaseEto getPurchase(long id) {

    return this.purchasemanagement.findPurchase(id);
  }

  @Override
  public PurchaseEto savePurchase(PurchaseEto purchase) {

    return this.purchasemanagement.savePurchase(purchase);
  }

  @Override
  public void deletePurchase(long id) {

    this.purchasemanagement.deletePurchase(id);
  }

  @Override
  public Page<PurchaseEto> findPurchases(PurchaseSearchCriteriaTo searchCriteriaTo) {

    return this.purchasemanagement.findPurchases(searchCriteriaTo);
  }

  @Override
  public PurchaseEto purchaseBook(PurchaseInCto purchaseInCto) {

    return this.purchasemanagement.purchaseBook(purchaseInCto);
  }
}