package com.devonfw.application.bb.clientmanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.devonfw.application.bb.clientmanagement.dataaccess.api.ClientEntity;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link ClientEntity}
 */
public interface ClientRepository extends DefaultRepository<ClientEntity> {

  /**
   * @param criteria the {@link ClientSearchCriteriaTo} with the criteria to search.
   * @return the {@link Page} of the {@link ClientEntity} objects that matched the search. If no pageable is set, it
   *         will return a unique page with all the objects that matched the search.
   */
  default Page<ClientEntity> findByCriteria(ClientSearchCriteriaTo criteria) {

    ClientEntity alias = newDslAlias();
    JPAQuery<ClientEntity> query = newDslQuery(alias);

    String name = criteria.getName();
    if (name != null && !name.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getName()), name, criteria.getNameOption());
    }
    String lastname = criteria.getLastname();
    if (lastname != null && !lastname.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getLastname()), lastname, criteria.getLastnameOption());
    }
    if (criteria.getPageable() == null) {
      criteria.setPageable(PageRequest.of(0, Integer.MAX_VALUE));
    } else {
      addOrderBy(query, alias, criteria.getPageable().getSort());
    }

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   * 
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<ClientEntity> query, ClientEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "name":
            if (next.isAscending()) {
              query.orderBy($(alias.getName()).asc());
            } else {
              query.orderBy($(alias.getName()).desc());
            }
            break;
          case "lastname":
            if (next.isAscending()) {
              query.orderBy($(alias.getLastname()).asc());
            } else {
              query.orderBy($(alias.getLastname()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}