package com.devonfw.application.bb.authormanagement.logic.base.usecase;

import javax.inject.Inject;

import com.devonfw.application.bb.authormanagement.dataaccess.api.repo.AuthorRepository;
import com.devonfw.application.bb.general.logic.base.AbstractUc;

/**
 * Abstract use case for Authors, which provides access to the commonly necessary data access objects.
 */
public class AbstractAuthorUc extends AbstractUc {

  /** @see #getAuthorRepository() */
  @Inject
  private AuthorRepository authorRepository;

  /**
   * Returns the field 'authorRepository'.
   * 
   * @return the {@link AuthorRepository} instance.
   */
  public AuthorRepository getAuthorRepository() {

    return this.authorRepository;
  }

}
