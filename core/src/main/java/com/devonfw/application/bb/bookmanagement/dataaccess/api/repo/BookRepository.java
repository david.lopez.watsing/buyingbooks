package com.devonfw.application.bb.bookmanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.util.Iterator;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devonfw.application.bb.bookmanagement.dataaccess.api.BookEntity;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link BookEntity}
 */
public interface BookRepository extends DefaultRepository<BookEntity> {

  /**
   * @param criteria the {@link BookSearchCriteriaTo} with the criteria to search.
   * @return the {@link Page} of the {@link BookEntity} objects that matched the search. If no pageable is set, it will
   *         return a unique page with all the objects that matched the search.
   */
  default Page<BookEntity> findByCriteria(BookSearchCriteriaTo criteria) {

    BookEntity alias = newDslAlias();
    JPAQuery<BookEntity> query = newDslQuery(alias);

    String name = criteria.getName();
    if (name != null && !name.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getName()), name, criteria.getNameOption());
    }
    Integer stock = criteria.getStock();
    if (stock != null) {
      query.where($(alias.getStock()).eq(stock));
    }
    Long authorEntity = criteria.getAuthorEntityId();
    if (authorEntity != null) {
      query.where($(alias.getAuthorEntity().getId()).eq(authorEntity));
    }
    if (criteria.getPageable() == null) {
      criteria.setPageable(PageRequest.of(0, Integer.MAX_VALUE));
    } else {
      addOrderBy(query, alias, criteria.getPageable().getSort());
    }

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   *
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<BookEntity> query, BookEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "name":
            if (next.isAscending()) {
              query.orderBy($(alias.getName()).asc());
            } else {
              query.orderBy($(alias.getName()).desc());
            }
            break;
          case "stock":
            if (next.isAscending()) {
              query.orderBy($(alias.getStock()).asc());
            } else {
              query.orderBy($(alias.getStock()).desc());
            }
            break;
          case "authorEntity":
            if (next.isAscending()) {
              query.orderBy($(alias.getAuthorEntity().getId()).asc());
            } else {
              query.orderBy($(alias.getAuthorEntity().getId()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

  /**
   * getBooksByClient
   * @param id
   * @return
   */
  @Query(value = "SELECT B.ID, B.MODIFICATIONCOUNTER, B.NAME, B.STOCK, B.IDAUTHOR from BOOK B JOIN PURCHASE P ON P.IDBOOK=B.ID WHERE P.ID=:id", nativeQuery = true)
  List<BookEntity> getBooksByClient(@Param("id") long id);

}