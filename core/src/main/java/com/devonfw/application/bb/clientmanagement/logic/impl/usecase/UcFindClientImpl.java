package com.devonfw.application.bb.clientmanagement.logic.impl.usecase;

import java.util.Optional;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.bb.clientmanagement.dataaccess.api.ClientEntity;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientEto;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientSearchCriteriaTo;
import com.devonfw.application.bb.clientmanagement.logic.api.usecase.UcFindClient;
import com.devonfw.application.bb.clientmanagement.logic.base.usecase.AbstractClientUc;

/**
 * Use case implementation for searching, filtering and getting Clients
 */
@Named
@Validated
@Transactional
public class UcFindClientImpl extends AbstractClientUc implements UcFindClient {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindClientImpl.class);

  @Override
  public ClientEto findClient(long id) {

    LOG.debug("Get Client with id {} from database.", id);
    Optional<ClientEntity> foundEntity = getClientRepository().findById(id);
    if (foundEntity.isPresent())
      return getBeanMapper().map(foundEntity.get(), ClientEto.class);
    else
      return null;
  }

  @Override
  public Page<ClientEto> findClients(ClientSearchCriteriaTo criteria) {

    Page<ClientEntity> clients = getClientRepository().findByCriteria(criteria);
    return mapPaginatedEntityList(clients, ClientEto.class);
  }

}
