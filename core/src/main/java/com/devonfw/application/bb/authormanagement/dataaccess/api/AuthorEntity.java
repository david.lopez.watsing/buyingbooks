package com.devonfw.application.bb.authormanagement.dataaccess.api;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.devonfw.application.bb.authormanagement.common.api.Author;
import com.devonfw.application.bb.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author dlopezwa
 */
@Entity
@Table(name = "Author")
public class AuthorEntity extends ApplicationPersistenceEntity implements Author {

  private static final long serialVersionUID = 1L;

  @NotNull
  @NotEmpty
  private String name;

  @NotNull
  @NotEmpty
  private String lastname;

  /**
   * The constructor.
   */
  public AuthorEntity() {

  }

  /**
   * The constructor.
   *
   * @param name
   * @param lastname
   */
  public AuthorEntity(@NotNull @NotEmpty String name, @NotNull @NotEmpty String lastname) {

    super();
    this.name = name;
    this.lastname = lastname;
  }

  /**
   * @return name
   */
  @Override
  public String getName() {

    return this.name;
  }

  /**
   * @param name new value of {@link #getname}.
   */
  @Override
  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return lastname
   */
  @Override
  public String getLastname() {

    return this.lastname;
  }

  /**
   * @param lastname new value of {@link #getlastname}.
   */
  @Override
  public void setLastname(String lastname) {

    this.lastname = lastname;
  }
}
