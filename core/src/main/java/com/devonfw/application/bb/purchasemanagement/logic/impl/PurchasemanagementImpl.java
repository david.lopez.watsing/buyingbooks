package com.devonfw.application.bb.purchasemanagement.logic.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

import com.devonfw.application.bb.general.logic.base.AbstractComponentFacade;
import com.devonfw.application.bb.purchasemanagement.logic.api.Purchasemanagement;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseEto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseInCto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseSearchCriteriaTo;
import com.devonfw.application.bb.purchasemanagement.logic.api.usecase.UcFindPurchase;
import com.devonfw.application.bb.purchasemanagement.logic.api.usecase.UcManagePurchase;

/**
 * Implementation of component interface of purchasemanagement
 */
@Named
public class PurchasemanagementImpl extends AbstractComponentFacade implements Purchasemanagement {

  @Inject
  private UcFindPurchase ucFindPurchase;

  @Inject
  private UcManagePurchase ucManagePurchase;

  private static final Logger LOG = LoggerFactory.getLogger(PurchasemanagementImpl.class);

  @Override
  public PurchaseEto findPurchase(long id) {

    return this.ucFindPurchase.findPurchase(id);
  }

  @Override
  public Page<PurchaseEto> findPurchases(PurchaseSearchCriteriaTo criteria) {

    return this.ucFindPurchase.findPurchases(criteria);
  }

  @Override
  public PurchaseEto savePurchase(PurchaseEto purchase) {

    return this.ucManagePurchase.savePurchase(purchase);
  }

  @Override
  public boolean deletePurchase(long id) {

    return this.ucManagePurchase.deletePurchase(id);
  }

  @Override
  public PurchaseEto purchaseBook(PurchaseInCto purchaseInCto) {

    LOG.debug(purchaseInCto.toString());
    return this.ucManagePurchase.purchaseBook(purchaseInCto);
  }
}
