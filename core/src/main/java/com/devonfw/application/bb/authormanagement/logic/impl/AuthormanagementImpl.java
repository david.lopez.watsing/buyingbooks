package com.devonfw.application.bb.authormanagement.logic.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.authormanagement.logic.api.Authormanagement;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorEto;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorSearchCriteriaTo;
import com.devonfw.application.bb.authormanagement.logic.api.usecase.UcFindAuthor;
import com.devonfw.application.bb.authormanagement.logic.api.usecase.UcManageAuthor;
import com.devonfw.application.bb.general.logic.base.AbstractComponentFacade;

/**
 * Implementation of component interface of authormanagement
 */
@Named
public class AuthormanagementImpl extends AbstractComponentFacade implements Authormanagement {

  @Inject
  private UcFindAuthor ucFindAuthor;

  @Inject
  private UcManageAuthor ucManageAuthor;

  @Override
  public AuthorEto findAuthor(long id) {

    return this.ucFindAuthor.findAuthor(id);
  }

  @Override
  public Page<AuthorEto> findAuthors(AuthorSearchCriteriaTo criteria) {

    return this.ucFindAuthor.findAuthors(criteria);
  }

  @Override
  public AuthorEto saveAuthor(AuthorEto author) {

    return this.ucManageAuthor.saveAuthor(author);
  }

  @Override
  public boolean deleteAuthor(long id) {

    return this.ucManageAuthor.deleteAuthor(id);
  }
}
