package com.devonfw.application.bb.purchasemanagement.logic.impl.usecase;

import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.bb.bookmanagement.logic.api.Bookmanagement;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookEto;
import com.devonfw.application.bb.purchasemanagement.dataaccess.api.PurchaseEntity;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseAdapter;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseEto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseInCto;
import com.devonfw.application.bb.purchasemanagement.logic.api.usecase.UcManagePurchase;
import com.devonfw.application.bb.purchasemanagement.logic.base.usecase.AbstractPurchaseUc;

/**
 * Use case implementation for modifying and deleting Purchases
 */
@Named
@Validated
@Transactional
public class UcManagePurchaseImpl extends AbstractPurchaseUc implements UcManagePurchase {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcManagePurchaseImpl.class);

  @Inject
  private Bookmanagement bookmanagement;

  @Override
  public boolean deletePurchase(long purchaseId) {

    PurchaseEntity purchase = getPurchaseRepository().find(purchaseId);
    getPurchaseRepository().delete(purchase);
    LOG.debug("The purchase with id '{}' has been deleted.", purchaseId);
    return true;
  }

  @Override
  public PurchaseEto savePurchase(PurchaseEto purchase) {

    Objects.requireNonNull(purchase, "purchase");

    PurchaseEntity purchaseEntity = getBeanMapper().map(purchase, PurchaseEntity.class);

    // initialize, validate purchaseEntity here if necessary
    PurchaseEntity resultEntity = getPurchaseRepository().save(purchaseEntity);
    LOG.debug("Purchase with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, PurchaseEto.class);
  }

  @Override
  public PurchaseEto purchaseBook(PurchaseInCto purchaseInCto) {

    // Verify Book Stock.
    BookEto book = this.bookmanagement.findBook(purchaseInCto.getBookId());

    // Only with stock
    if (book.getStock() > 0) {
      // Save the Purchase.
      PurchaseEto purchaseEto = savePurchase(PurchaseAdapter.parseToPurchaseEto(purchaseInCto));

      // Decrease Book Stock.
      book.setStock(book.getStock() - 1);
      this.bookmanagement.saveBook(book);

      return purchaseEto;
    }

    return null;
  }

}
