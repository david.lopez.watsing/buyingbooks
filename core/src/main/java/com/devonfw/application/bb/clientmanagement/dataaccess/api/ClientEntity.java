package com.devonfw.application.bb.clientmanagement.dataaccess.api;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.devonfw.application.bb.clientmanagement.common.api.Client;
import com.devonfw.application.bb.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author dlopezwa
 */
@Entity
@Table(name = "Client")
public class ClientEntity extends ApplicationPersistenceEntity implements Client {

  private static final long serialVersionUID = 1L;

  @NotNull
  @NotEmpty
  private String name;

  @NotNull
  @NotEmpty
  private String lastname;

  /**
   * The constructor.
   */
  public ClientEntity() {

    super();
  }

  /**
   * The constructor.
   *
   * @param id
   * @param name
   * @param lastname
   */
  public ClientEntity(@NotNull @NotEmpty String name, @NotNull @NotEmpty String lastname) {

    super();
    this.name = name;
    this.lastname = lastname;
  }

  /**
   * @return name
   */
  @Override
  public String getName() {

    return this.name;
  }

  /**
   * @param name new value of {@link #getname}.
   */
  @Override
  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return lastname
   */
  @Override
  public String getLastname() {

    return this.lastname;
  }

  /**
   * @param lastname new value of {@link #getlastname}.
   */
  @Override
  public void setLastname(String lastname) {

    this.lastname = lastname;
  }

}
