package com.devonfw.application.bb.bookmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.devonfw.application.bb.bookmanagement.dataaccess.api.repo.BookRepository;
import com.devonfw.application.bb.general.logic.base.AbstractUc;

/**
 * Abstract use case for Books, which provides access to the commonly necessary data access objects.
 */
public class AbstractBookUc extends AbstractUc {

  /** @see #getBookRepository() */
  @Inject
  private BookRepository bookRepository;

  /**
   * Returns the field 'bookRepository'.
   * 
   * @return the {@link BookRepository} instance.
   */
  public BookRepository getBookRepository() {

    return this.bookRepository;
  }

}
