package com.devonfw.application.bb.purchasemanagement.logic.base.usecase;

import javax.inject.Inject;

import com.devonfw.application.bb.general.logic.base.AbstractUc;
import com.devonfw.application.bb.purchasemanagement.dataaccess.api.repo.PurchaseRepository;

/**
 * Abstract use case for Purchases, which provides access to the commonly necessary data access objects.
 */
public class AbstractPurchaseUc extends AbstractUc {

  /** @see #getPurchaseRepository() */
  @Inject
  private PurchaseRepository purchaseRepository;

  /**
   * Returns the field 'purchaseRepository'.
   * 
   * @return the {@link PurchaseRepository} instance.
   */
  public PurchaseRepository getPurchaseRepository() {

    return this.purchaseRepository;
  }

}
