package com.devonfw.application.bb.purchasemanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.sql.Timestamp;
import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.devonfw.application.bb.purchasemanagement.dataaccess.api.PurchaseEntity;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link PurchaseEntity}
 */
public interface PurchaseRepository extends DefaultRepository<PurchaseEntity> {

  /**
   * @param criteria the {@link PurchaseSearchCriteriaTo} with the criteria to search.
   * @return the {@link Page} of the {@link PurchaseEntity} objects that matched the search. If no pageable is set, it
   *         will return a unique page with all the objects that matched the search.
   */
  default Page<PurchaseEntity> findByCriteria(PurchaseSearchCriteriaTo criteria) {

    PurchaseEntity alias = newDslAlias();
    JPAQuery<PurchaseEntity> query = newDslQuery(alias);

    Long client = criteria.getClientId();
    if (client != null) {
      query.where($(alias.getClientEntity().getId()).eq(client));
    }
    Long book = criteria.getBookId();
    if (book != null) {
      query.where($(alias.getBook().getId()).eq(book));
    }
    Timestamp purchaseDate = criteria.getPurchaseDate();
    if (purchaseDate != null) {
      query.where($(alias.getPurchaseDate()).eq(purchaseDate));
    }
    if (criteria.getPageable() == null) {
      criteria.setPageable(PageRequest.of(0, Integer.MAX_VALUE));
    } else {
      addOrderBy(query, alias, criteria.getPageable().getSort());
    }

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   *
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<PurchaseEntity> query, PurchaseEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "client":
            if (next.isAscending()) {
              query.orderBy($(alias.getClientEntity().getId()).asc());
            } else {
              query.orderBy($(alias.getClientEntity().getId()).desc());
            }
            break;
          case "book":
            if (next.isAscending()) {
              query.orderBy($(alias.getBook().getId()).asc());
            } else {
              query.orderBy($(alias.getBook().getId()).desc());
            }
            break;
          case "purchaseDate":
            if (next.isAscending()) {
              query.orderBy($(alias.getPurchaseDate()).asc());
            } else {
              query.orderBy($(alias.getPurchaseDate()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}