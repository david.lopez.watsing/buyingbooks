package com.devonfw.application.bb.bookmanagement.service.impl.rest;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

import com.devonfw.application.bb.bookmanagement.logic.api.Bookmanagement;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookEto;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookSearchCriteriaTo;
import com.devonfw.application.bb.bookmanagement.service.api.rest.BookmanagementRestService;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Bookmanagement}.
 */
@Named("BookmanagementRestService")
public class BookmanagementRestServiceImpl implements BookmanagementRestService {

  @Inject
  private Bookmanagement bookmanagement;

  private static final Logger LOG = LoggerFactory.getLogger(BookmanagementRestServiceImpl.class);

  @Override
  public BookEto getBook(long id) {

    return this.bookmanagement.findBook(id);
  }

  @Override
  public BookEto saveBook(BookEto book) {

    return this.bookmanagement.saveBook(book);
  }

  @Override
  public void deleteBook(long id) {

    this.bookmanagement.deleteBook(id);
  }

  @Override
  public Page<BookEto> findBooks(BookSearchCriteriaTo searchCriteriaTo) {

    return this.bookmanagement.findBooks(searchCriteriaTo);
  }

  @Override
  public List<BookEto> getBooksByClient(long id) {

    LOG.info("Inicio getBookByClient:" + id);
    return this.bookmanagement.getBooksByClient(id);
  }
}