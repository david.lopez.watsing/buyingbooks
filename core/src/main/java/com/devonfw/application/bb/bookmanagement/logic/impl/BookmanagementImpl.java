package com.devonfw.application.bb.bookmanagement.logic.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.bookmanagement.logic.api.Bookmanagement;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookEto;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookSearchCriteriaTo;
import com.devonfw.application.bb.bookmanagement.logic.api.usecase.UcFindBook;
import com.devonfw.application.bb.bookmanagement.logic.api.usecase.UcManageBook;
import com.devonfw.application.bb.general.logic.base.AbstractComponentFacade;

/**
 * Implementation of component interface of bookmanagement
 */
@Named
public class BookmanagementImpl extends AbstractComponentFacade implements Bookmanagement {

  @Inject
  private UcFindBook ucFindBook;

  @Inject
  private UcManageBook ucManageBook;

  @Override
  public BookEto findBook(long id) {

    return this.ucFindBook.findBook(id);
  }

  @Override
  public Page<BookEto> findBooks(BookSearchCriteriaTo criteria) {

    return this.ucFindBook.findBooks(criteria);
  }

  @Override
  public BookEto saveBook(BookEto book) {

    return this.ucManageBook.saveBook(book);
  }

  @Override
  public boolean deleteBook(long id) {

    return this.ucManageBook.deleteBook(id);
  }

  @Override
  public List<BookEto> getBooksByClient(long id) {

    return this.ucFindBook.getBooksByClient(id);
  }
}
