package com.devonfw.application.bb.clientmanagement.logic.impl.usecase;

import java.util.Objects;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.bb.clientmanagement.dataaccess.api.ClientEntity;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientEto;
import com.devonfw.application.bb.clientmanagement.logic.api.usecase.UcManageClient;
import com.devonfw.application.bb.clientmanagement.logic.base.usecase.AbstractClientUc;

/**
 * Use case implementation for modifying and deleting Clients
 */
@Named
@Validated
@Transactional
public class UcManageClientImpl extends AbstractClientUc implements UcManageClient {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcManageClientImpl.class);

  @Override
  public boolean deleteClient(long clientId) {

    ClientEntity client = getClientRepository().find(clientId);
    getClientRepository().delete(client);
    LOG.debug("The client with id '{}' has been deleted.", clientId);
    return true;
  }

  @Override
  public ClientEto saveClient(ClientEto client) {

    Objects.requireNonNull(client, "client");

    ClientEntity clientEntity = getBeanMapper().map(client, ClientEntity.class);

    // initialize, validate clientEntity here if necessary
    ClientEntity resultEntity = getClientRepository().save(clientEntity);
    LOG.debug("Client with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, ClientEto.class);
  }
}
