package com.devonfw.application.bb.authormanagement.logic.impl.usecase;

import java.util.Optional;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.bb.authormanagement.dataaccess.api.AuthorEntity;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorEto;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorSearchCriteriaTo;
import com.devonfw.application.bb.authormanagement.logic.api.usecase.UcFindAuthor;
import com.devonfw.application.bb.authormanagement.logic.base.usecase.AbstractAuthorUc;

/**
 * Use case implementation for searching, filtering and getting Authors
 */
@Named
@Validated
@Transactional
public class UcFindAuthorImpl extends AbstractAuthorUc implements UcFindAuthor {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindAuthorImpl.class);

  @Override
  public AuthorEto findAuthor(long id) {

    LOG.debug("Get Author with id {} from database.", id);
    Optional<AuthorEntity> foundEntity = getAuthorRepository().findById(id);
    if (foundEntity.isPresent())
      return getBeanMapper().map(foundEntity.get(), AuthorEto.class);
    else
      return null;
  }

  @Override
  public Page<AuthorEto> findAuthors(AuthorSearchCriteriaTo criteria) {

    Page<AuthorEntity> authors = getAuthorRepository().findByCriteria(criteria);
    return mapPaginatedEntityList(authors, AuthorEto.class);
  }

}
