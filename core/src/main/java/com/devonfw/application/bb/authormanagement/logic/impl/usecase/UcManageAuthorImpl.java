package com.devonfw.application.bb.authormanagement.logic.impl.usecase;

import java.util.Objects;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.bb.authormanagement.dataaccess.api.AuthorEntity;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorEto;
import com.devonfw.application.bb.authormanagement.logic.api.usecase.UcManageAuthor;
import com.devonfw.application.bb.authormanagement.logic.base.usecase.AbstractAuthorUc;

/**
 * Use case implementation for modifying and deleting Authors
 */
@Named
@Validated
@Transactional
public class UcManageAuthorImpl extends AbstractAuthorUc implements UcManageAuthor {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcManageAuthorImpl.class);

  @Override
  public boolean deleteAuthor(long authorId) {

    AuthorEntity author = getAuthorRepository().find(authorId);
    getAuthorRepository().delete(author);
    LOG.debug("The author with id '{}' has been deleted.", authorId);
    return true;
  }

  @Override
  public AuthorEto saveAuthor(AuthorEto author) {

    Objects.requireNonNull(author, "author");

    AuthorEntity authorEntity = getBeanMapper().map(author, AuthorEntity.class);

    // initialize, validate authorEntity here if necessary
    AuthorEntity resultEntity = getAuthorRepository().save(authorEntity);
    LOG.debug("Author with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, AuthorEto.class);
  }
}
