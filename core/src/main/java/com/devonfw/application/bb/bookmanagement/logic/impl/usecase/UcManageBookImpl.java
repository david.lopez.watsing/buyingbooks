package com.devonfw.application.bb.bookmanagement.logic.impl.usecase;

import java.util.Objects;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.bb.bookmanagement.dataaccess.api.BookEntity;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookEto;
import com.devonfw.application.bb.bookmanagement.logic.api.usecase.UcManageBook;
import com.devonfw.application.bb.bookmanagement.logic.base.usecase.AbstractBookUc;

/**
 * Use case implementation for modifying and deleting Books
 */
@Named
@Validated
@Transactional
public class UcManageBookImpl extends AbstractBookUc implements UcManageBook {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcManageBookImpl.class);

  @Override
  public boolean deleteBook(long bookId) {

    BookEntity book = getBookRepository().find(bookId);
    getBookRepository().delete(book);
    LOG.debug("The book with id '{}' has been deleted.", bookId);
    return true;
  }

  @Override
  public BookEto saveBook(BookEto book) {

    Objects.requireNonNull(book, "book");

    BookEntity bookEntity = getBeanMapper().map(book, BookEntity.class);

    // initialize, validate bookEntity here if necessary
    BookEntity resultEntity = getBookRepository().save(bookEntity);
    LOG.debug("Book with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, BookEto.class);
  }
}
