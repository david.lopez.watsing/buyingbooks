package com.devonfw.application.bb.clientmanagement.logic.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.clientmanagement.logic.api.Clientmanagement;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientEto;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientSearchCriteriaTo;
import com.devonfw.application.bb.clientmanagement.logic.api.usecase.UcFindClient;
import com.devonfw.application.bb.clientmanagement.logic.api.usecase.UcManageClient;
import com.devonfw.application.bb.general.logic.base.AbstractComponentFacade;

/**
 * Implementation of component interface of clientmanagement
 */
@Named
public class ClientmanagementImpl extends AbstractComponentFacade implements Clientmanagement {

  @Inject
  private UcFindClient ucFindClient;

  @Inject
  private UcManageClient ucManageClient;

  @Override
  public ClientEto findClient(long id) {

    return this.ucFindClient.findClient(id);
  }

  @Override
  public Page<ClientEto> findClients(ClientSearchCriteriaTo criteria) {

    return this.ucFindClient.findClients(criteria);
  }

  @Override
  public ClientEto saveClient(ClientEto client) {

    return this.ucManageClient.saveClient(client);
  }

  @Override
  public boolean deleteClient(long id) {

    return this.ucManageClient.deleteClient(id);
  }
}
