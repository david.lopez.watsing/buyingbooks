package com.devonfw.application.bb.authormanagement.logic.api.usecase;

import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorEto;

/**
 * Interface of UcManageAuthor to centralize documentation and signatures of methods.
 */
public interface UcManageAuthor {

  /**
   * Deletes a author from the database by its id 'authorId'.
   *
   * @param authorId Id of the author to delete
   * @return boolean <code>true</code> if the author can be deleted, <code>false</code> otherwise
   */
  boolean deleteAuthor(long authorId);

  /**
   * Saves a author and store it in the database.
   *
   * @param author the {@link AuthorEto} to create.
   * @return the new {@link AuthorEto} that has been saved with ID and version.
   */
  AuthorEto saveAuthor(AuthorEto author);

}
