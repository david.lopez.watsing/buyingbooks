package com.devonfw.application.bb.purchasemanagement.logic.api.usecase;

import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseEto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseInCto;

/**
 * Interface of UcManagePurchase to centralize documentation and signatures of methods.
 */
public interface UcManagePurchase {

  /**
   * Deletes a purchase from the database by its id 'purchaseId'.
   *
   * @param purchaseId Id of the purchase to delete
   * @return boolean <code>true</code> if the purchase can be deleted, <code>false</code> otherwise
   */
  boolean deletePurchase(long purchaseId);

  /**
   * Saves a purchase and store it in the database.
   *
   * @param purchase the {@link PurchaseEto} to create.
   * @return the new {@link PurchaseEto} that has been saved with ID and version.
   */
  PurchaseEto savePurchase(PurchaseEto purchase);

  /**
   * Purchase book.
   *
   * @param purchaseInCto the purchase in cto {@link PurchaseInCto}
   * @return the purchase eto {@link PurchaseEto}
   */
  PurchaseEto purchaseBook(PurchaseInCto purchaseInCto);
}
