package com.devonfw.application.bb.purchasemanagement.common.api;

import java.sql.Timestamp;

import com.devonfw.application.bb.general.common.api.ApplicationEntity;

public interface Purchase extends ApplicationEntity {

  /**
   * getter for clientId attribute
   *
   * @return clientId
   */

  public Long getClientId();

  /**
   * @param client setter for client attribute
   */

  public void setClientId(Long clientId);

  /**
   * getter for bookId attribute
   *
   * @return bookId
   */

  public Long getBookId();

  /**
   * @param book setter for book attribute
   */

  public void setBookId(Long bookId);

  /**
   * @return purchaseDateId
   */

  public Timestamp getPurchaseDate();

  /**
   * @param purchaseDate setter for purchaseDate attribute
   */

  public void setPurchaseDate(Timestamp purchaseDate);

}
