package com.devonfw.application.bb.clientmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientEto;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientSearchCriteriaTo;

public interface UcFindClient {

  /**
   * Returns a Client by its id 'id'.
   *
   * @param id The id 'id' of the Client.
   * @return The {@link ClientEto} with id 'id'
   */
  ClientEto findClient(long id);

  /**
   * Returns a paginated list of Clients matching the search criteria.
   *
   * @param criteria the {@link ClientSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ClientEto}s.
   */
  Page<ClientEto> findClients(ClientSearchCriteriaTo criteria);

}
