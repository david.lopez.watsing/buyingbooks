package com.devonfw.application.bb.bookmanagement.logic.api.usecase;

import com.devonfw.application.bb.bookmanagement.logic.api.to.BookEto;

/**
 * Interface of UcManageBook to centralize documentation and signatures of methods.
 */
public interface UcManageBook {

  /**
   * Deletes a book from the database by its id 'bookId'.
   *
   * @param bookId Id of the book to delete
   * @return boolean <code>true</code> if the book can be deleted, <code>false</code> otherwise
   */
  boolean deleteBook(long bookId);

  /**
   * Saves a book and store it in the database.
   *
   * @param book the {@link BookEto} to create.
   * @return the new {@link BookEto} that has been saved with ID and version.
   */
  BookEto saveBook(BookEto book);

}
