package com.devonfw.application.bb.purchasemanagement.logic.api.to;

import java.sql.Timestamp;

import com.devonfw.application.bb.general.common.api.to.AbstractSearchCriteriaTo;

/**
 * {@link SearchCriteriaTo} to find instances of
 * {@link com.devonfw.application.bb.purchasemanagement.common.api.Purchase}s.
 */
public class PurchaseSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Long clientId;

  private Long bookId;

  private Timestamp purchaseDate;

  /**
   * getter for clientId attribute
   *
   * @return clientId
   */

  public Long getClientId() {

    return this.clientId;
  }

  /**
   * @param client setter for client attribute
   */

  public void setClientId(Long clientId) {

    this.clientId = clientId;
  }

  /**
   * getter for bookId attribute
   *
   * @return bookId
   */

  public Long getBookId() {

    return this.bookId;
  }

  /**
   * @param book setter for book attribute
   */

  public void setBookId(Long bookId) {

    this.bookId = bookId;
  }

  /**
   * @return purchaseDateId
   */

  public Timestamp getPurchaseDate() {

    return this.purchaseDate;
  }

  /**
   * @param purchaseDate setter for purchaseDate attribute
   */

  public void setPurchaseDate(Timestamp purchaseDate) {

    this.purchaseDate = purchaseDate;
  }

}
