package com.devonfw.application.bb.purchasemanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.purchasemanagement.logic.api.Purchasemanagement;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseEto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseInCto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseSearchCriteriaTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Purchasemanagement}.
 */
@Path("/purchasemanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface PurchasemanagementRestService {

  /**
   * Delegates to {@link Purchasemanagement#findPurchase}.
   *
   * @param id the ID of the {@link PurchaseEto}
   * @return the {@link PurchaseEto}
   */
  @GET
  @Path("/purchase/{id}/")
  public PurchaseEto getPurchase(@PathParam("id") long id);

  /**
   * Delegates to {@link Purchasemanagement#savePurchase}.
   *
   * @param purchase the {@link PurchaseEto} to be saved
   * @return the recently created {@link PurchaseEto}
   */
  @POST
  @Path("/purchase/")
  public PurchaseEto savePurchase(PurchaseEto purchase);

  /**
   * Delegates to {@link Purchasemanagement#deletePurchase}.
   *
   * @param id ID of the {@link PurchaseEto} to be deleted
   */
  @DELETE
  @Path("/purchase/{id}/")
  public void deletePurchase(@PathParam("id") long id);

  /**
   * Delegates to {@link Purchasemanagement#findPurchaseEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding purchases.
   * @return the {@link Page list} of matching {@link PurchaseEto}s.
   */
  @Path("/purchase/search")
  @POST
  public Page<PurchaseEto> findPurchases(PurchaseSearchCriteriaTo searchCriteriaTo);

  /**
   * Purchase book. Delegates to {@link Purchasemanagement#purchaseBook}.
   *
   * @param purchaseInCto the purchase in cto
   * @return the purchase eto
   */
  @Path("/purchase/book")
  @POST
  public PurchaseEto purchaseBook(PurchaseInCto purchaseInCto);

}