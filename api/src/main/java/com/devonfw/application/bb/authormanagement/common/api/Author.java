package com.devonfw.application.bb.authormanagement.common.api;

import com.devonfw.application.bb.general.common.api.ApplicationEntity;

public interface Author extends ApplicationEntity {

  /**
   * @return nameId
   */

  public String getName();

  /**
   * @param name setter for name attribute
   */

  public void setName(String name);

  /**
   * @return lastnameId
   */

  public String getLastname();

  /**
   * @param lastname setter for lastname attribute
   */

  public void setLastname(String lastname);

}
