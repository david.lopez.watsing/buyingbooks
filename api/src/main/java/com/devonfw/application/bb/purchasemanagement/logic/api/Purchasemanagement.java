package com.devonfw.application.bb.purchasemanagement.logic.api;

import com.devonfw.application.bb.purchasemanagement.logic.api.usecase.UcFindPurchase;
import com.devonfw.application.bb.purchasemanagement.logic.api.usecase.UcManagePurchase;

/**
 * Interface for Purchasemanagement component.
 */
public interface Purchasemanagement extends UcFindPurchase, UcManagePurchase {

}
