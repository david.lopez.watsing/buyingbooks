package com.devonfw.application.bb.purchasemanagement.logic.api.to;

import java.sql.Timestamp;

import com.devonfw.application.bb.purchasemanagement.common.api.Purchase;
import com.devonfw.module.basic.common.api.to.AbstractEto;

/**
 * Entity transport object of Purchase
 */
public class PurchaseEto extends AbstractEto implements Purchase {

  private static final long serialVersionUID = 1L;

  private Long clientId;

  private Long bookId;

  private Timestamp purchaseDate;

  @Override
  public Long getClientId() {

    return this.clientId;
  }

  @Override
  public void setClientId(Long clientId) {

    this.clientId = clientId;
  }

  @Override
  public Long getBookId() {

    return this.bookId;
  }

  @Override
  public void setBookId(Long bookId) {

    this.bookId = bookId;
  }

  @Override
  public Timestamp getPurchaseDate() {

    return this.purchaseDate;
  }

  @Override
  public void setPurchaseDate(Timestamp purchaseDate) {

    this.purchaseDate = purchaseDate;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();

    result = prime * result + ((this.clientId == null) ? 0 : this.clientId.hashCode());

    result = prime * result + ((this.bookId == null) ? 0 : this.bookId.hashCode());
    result = prime * result + ((this.purchaseDate == null) ? 0 : this.purchaseDate.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    PurchaseEto other = (PurchaseEto) obj;

    if (this.clientId == null) {
      if (other.clientId != null) {
        return false;
      }
    } else if (!this.clientId.equals(other.clientId)) {
      return false;
    }

    if (this.bookId == null) {
      if (other.bookId != null) {
        return false;
      }
    } else if (!this.bookId.equals(other.bookId)) {
      return false;
    }
    if (this.purchaseDate == null) {
      if (other.purchaseDate != null) {
        return false;
      }
    } else if (!this.purchaseDate.equals(other.purchaseDate)) {
      return false;
    }
    return true;
  }
}
