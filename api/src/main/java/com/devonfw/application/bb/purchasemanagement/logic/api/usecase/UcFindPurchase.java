package com.devonfw.application.bb.purchasemanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseEto;
import com.devonfw.application.bb.purchasemanagement.logic.api.to.PurchaseSearchCriteriaTo;

public interface UcFindPurchase {

  /**
   * Returns a Purchase by its id 'id'.
   *
   * @param id The id 'id' of the Purchase.
   * @return The {@link PurchaseEto} with id 'id'
   */
  PurchaseEto findPurchase(long id);

  /**
   * Returns a paginated list of Purchases matching the search criteria.
   *
   * @param criteria the {@link PurchaseSearchCriteriaTo}.
   * @return the {@link List} of matching {@link PurchaseEto}s.
   */
  Page<PurchaseEto> findPurchases(PurchaseSearchCriteriaTo criteria);

}
