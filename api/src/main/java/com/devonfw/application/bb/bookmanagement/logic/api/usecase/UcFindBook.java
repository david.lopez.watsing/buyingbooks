package com.devonfw.application.bb.bookmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.bookmanagement.logic.api.to.BookEto;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookSearchCriteriaTo;

public interface UcFindBook {

  /**
   * Returns a Book by its id 'id'.
   *
   * @param id The id 'id' of the Book.
   * @return The {@link BookEto} with id 'id'
   */
  BookEto findBook(long id);

  /**
   * Returns a paginated list of Books matching the search criteria.
   *
   * @param criteria the {@link BookSearchCriteriaTo}.
   * @return the {@link List} of matching {@link BookEto}s.
   */
  Page<BookEto> findBooks(BookSearchCriteriaTo criteria);

  /**
   * Returns a list of Books matching the Client.
   *
   * @param id The id 'id' of the Client.
   * @return the {@link List} of matching {@link BookEto}s.
   */
  List<BookEto> getBooksByClient(long id);
}
