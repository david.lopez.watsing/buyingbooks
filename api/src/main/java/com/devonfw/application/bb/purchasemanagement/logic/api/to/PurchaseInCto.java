package com.devonfw.application.bb.purchasemanagement.logic.api.to;

/**
 * The Class PurchaseBuyCto.
 */
public class PurchaseInCto {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The client id. */
  private Long clientId;

  /** The book id. */
  private Long bookId;

  /**
   * Gets the client id.
   *
   * @return the client id
   */
  public Long getClientId() {

    return this.clientId;
  }

  /**
   * Sets the client id.
   *
   * @param clientId the new client id
   */
  public void setClientId(Long clientId) {

    this.clientId = clientId;
  }

  /**
   * Gets the book id.
   *
   * @return the book id
   */
  public Long getBookId() {

    return this.bookId;
  }

  /**
   * Sets the book id.
   *
   * @param bookId the new book id
   */
  public void setBookId(Long bookId) {

    this.bookId = bookId;
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();

    result = prime * result + ((this.clientId == null) ? 0 : this.clientId.hashCode());

    result = prime * result + ((this.bookId == null) ? 0 : this.bookId.hashCode());
    return result;
  }

  /**
   * Equals.
   *
   * @param obj the obj
   * @return true, if successful
   */
  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    PurchaseInCto other = (PurchaseInCto) obj;

    if (this.clientId == null) {
      if (other.clientId != null) {
        return false;
      }
    } else if (!this.clientId.equals(other.clientId)) {
      return false;
    }

    if (this.bookId == null) {
      if (other.bookId != null) {
        return false;
      }
    } else if (!this.bookId.equals(other.bookId)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {

    return "PurchaseInCto [clientId=" + this.clientId + ", bookId=" + this.bookId + "]";
  }

}
