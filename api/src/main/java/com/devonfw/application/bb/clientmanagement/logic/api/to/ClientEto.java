package com.devonfw.application.bb.clientmanagement.logic.api.to;

import com.devonfw.application.bb.clientmanagement.common.api.Client;
import com.devonfw.module.basic.common.api.to.AbstractEto;

/**
 * Entity transport object of Client
 */
public class ClientEto extends AbstractEto implements Client {

  private static final long serialVersionUID = 1L;

  private String name;

  private String lastname;

  @Override
  public String getName() {

    return name;
  }

  @Override
  public void setName(String name) {

    this.name = name;
  }

  @Override
  public String getLastname() {

    return lastname;
  }

  @Override
  public void setLastname(String lastname) {

    this.lastname = lastname;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
    result = prime * result + ((this.lastname == null) ? 0 : this.lastname.hashCode());

    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    ClientEto other = (ClientEto) obj;
    if (this.name == null) {
      if (other.name != null) {
        return false;
      }
    } else if (!this.name.equals(other.name)) {
      return false;
    }
    if (this.lastname == null) {
      if (other.lastname != null) {
        return false;
      }
    } else if (!this.lastname.equals(other.lastname)) {
      return false;
    }

    return true;
  }
}
