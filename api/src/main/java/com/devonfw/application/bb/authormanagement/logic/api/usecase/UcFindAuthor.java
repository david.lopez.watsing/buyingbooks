package com.devonfw.application.bb.authormanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorEto;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorSearchCriteriaTo;

public interface UcFindAuthor {

  /**
   * Returns a Author by its id 'id'.
   *
   * @param id The id 'id' of the Author.
   * @return The {@link AuthorEto} with id 'id'
   */
  AuthorEto findAuthor(long id);

  /**
   * Returns a paginated list of Authors matching the search criteria.
   *
   * @param criteria the {@link AuthorSearchCriteriaTo}.
   * @return the {@link List} of matching {@link AuthorEto}s.
   */
  Page<AuthorEto> findAuthors(AuthorSearchCriteriaTo criteria);

}
