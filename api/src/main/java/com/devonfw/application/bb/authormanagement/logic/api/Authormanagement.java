package com.devonfw.application.bb.authormanagement.logic.api;

import com.devonfw.application.bb.authormanagement.logic.api.usecase.UcFindAuthor;
import com.devonfw.application.bb.authormanagement.logic.api.usecase.UcManageAuthor;

/**
 * Interface for Authormanagement component.
 */
public interface Authormanagement extends UcFindAuthor, UcManageAuthor {

}
