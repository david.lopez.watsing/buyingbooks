package com.devonfw.application.bb.bookmanagement.common.api;

import com.devonfw.application.bb.general.common.api.ApplicationEntity;

public interface Book extends ApplicationEntity {

  /**
   * @return nameId
   */

  public String getName();

  /**
   * @param name setter for name attribute
   */

  public void setName(String name);

  /**
   * @return stockId
   */

  public int getStock();

  /**
   * @param stock setter for stock attribute
   */

  public void setStock(int stock);

  /**
   * getter for authorEntityId attribute
   * 
   * @return authorEntityId
   */

  public Long getAuthorEntityId();

  /**
   * @param authorEntity setter for authorEntity attribute
   */

  public void setAuthorEntityId(Long authorEntityId);

}
