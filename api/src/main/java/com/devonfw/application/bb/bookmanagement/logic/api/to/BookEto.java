package com.devonfw.application.bb.bookmanagement.logic.api.to;

import com.devonfw.application.bb.bookmanagement.common.api.Book;
import com.devonfw.module.basic.common.api.to.AbstractEto;

/**
 * Entity transport object of Book
 */
public class BookEto extends AbstractEto implements Book {

  private static final long serialVersionUID = 1L;

  private String name;

  private int stock;

  private Long authorEntityId;

  @Override
  public String getName() {

    return name;
  }

  @Override
  public void setName(String name) {

    this.name = name;
  }

  @Override
  public int getStock() {

    return stock;
  }

  @Override
  public void setStock(int stock) {

    this.stock = stock;
  }

  @Override
  public Long getAuthorEntityId() {

    return authorEntityId;
  }

  @Override
  public void setAuthorEntityId(Long authorEntityId) {

    this.authorEntityId = authorEntityId;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
    result = prime * result + ((Integer) stock).hashCode();

    result = prime * result + ((this.authorEntityId == null) ? 0 : this.authorEntityId.hashCode());

    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    BookEto other = (BookEto) obj;
    if (this.name == null) {
      if (other.name != null) {
        return false;
      }
    } else if (!this.name.equals(other.name)) {
      return false;
    }
    if (this.stock != other.stock) {
      return false;
    }

    if (this.authorEntityId == null) {
      if (other.authorEntityId != null) {
        return false;
      }
    } else if (!this.authorEntityId.equals(other.authorEntityId)) {
      return false;
    }

    return true;
  }
}
