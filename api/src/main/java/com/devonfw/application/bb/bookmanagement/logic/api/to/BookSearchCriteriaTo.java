package com.devonfw.application.bb.bookmanagement.logic.api.to;

import com.devonfw.application.bb.general.common.api.to.AbstractSearchCriteriaTo;
import com.devonfw.module.basic.common.api.query.StringSearchConfigTo;

/**
 * {@link SearchCriteriaTo} to find instances of {@link com.devonfw.application.bb.bookmanagement.common.api.Book}s.
 */
public class BookSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String name;

  private Integer stock;

  private Long authorEntityId;

  private StringSearchConfigTo nameOption;

  /**
   * @return nameId
   */

  public String getName() {

    return name;
  }

  /**
   * @param name setter for name attribute
   */

  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return stockId
   */

  public Integer getStock() {

    return stock;
  }

  /**
   * @param stock setter for stock attribute
   */

  public void setStock(Integer stock) {

    this.stock = stock;
  }

  /**
   * getter for authorEntityId attribute
   * 
   * @return authorEntityId
   */

  public Long getAuthorEntityId() {

    return authorEntityId;
  }

  /**
   * @param authorEntity setter for authorEntity attribute
   */

  public void setAuthorEntityId(Long authorEntityId) {

    this.authorEntityId = authorEntityId;
  }

  /**
   * @return the {@link StringSearchConfigTo} used to search for {@link #getName() name}.
   */
  public StringSearchConfigTo getNameOption() {

    return this.nameOption;
  }

  /**
   * @param nameOption new value of {@link #getNameOption()}.
   */
  public void setNameOption(StringSearchConfigTo nameOption) {

    this.nameOption = nameOption;
  }

}
