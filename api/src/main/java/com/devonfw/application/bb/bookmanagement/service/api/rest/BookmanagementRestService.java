package com.devonfw.application.bb.bookmanagement.service.api.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.bookmanagement.logic.api.Bookmanagement;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookEto;
import com.devonfw.application.bb.bookmanagement.logic.api.to.BookSearchCriteriaTo;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientEto;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Bookmanagement}.
 */
@Path("/bookmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface BookmanagementRestService {

  /**
   * Delegates to {@link Bookmanagement#findBook}.
   *
   * @param id the ID of the {@link BookEto}
   * @return the {@link BookEto}
   */
  @GET
  @Path("/book/{id}/")
  public BookEto getBook(@PathParam("id") long id);

  /**
   * Delegates to {@link Bookmanagement#saveBook}.
   *
   * @param book the {@link BookEto} to be saved
   * @return the recently created {@link BookEto}
   */
  @POST
  @Path("/book/")
  public BookEto saveBook(BookEto book);

  /**
   * Delegates to {@link Bookmanagement#deleteBook}.
   *
   * @param id ID of the {@link BookEto} to be deleted
   */
  @DELETE
  @Path("/book/{id}/")
  public void deleteBook(@PathParam("id") long id);

  /**
   * Delegates to {@link Bookmanagement#findBookEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding books.
   * @return the {@link Page list} of matching {@link BookEto}s.
   */
  @Path("/book/search")
  @POST
  public Page<BookEto> findBooks(BookSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Bookmanagement#getBooksByClient}.
   *
   * @param id the ID of the {@link ClientEto}
   * @return the {@link list} of matching {@link BookEto}s.
   */
  @GET
  @Path("/book/client/{id}/")
  public List<BookEto> getBooksByClient(@PathParam("id") long id);

}