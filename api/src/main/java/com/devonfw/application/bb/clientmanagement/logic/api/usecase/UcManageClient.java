package com.devonfw.application.bb.clientmanagement.logic.api.usecase;

import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientEto;

/**
 * Interface of UcManageClient to centralize documentation and signatures of methods.
 */
public interface UcManageClient {

  /**
   * Deletes a client from the database by its id 'clientId'.
   *
   * @param clientId Id of the client to delete
   * @return boolean <code>true</code> if the client can be deleted, <code>false</code> otherwise
   */
  boolean deleteClient(long clientId);

  /**
   * Saves a client and store it in the database.
   *
   * @param client the {@link ClientEto} to create.
   * @return the new {@link ClientEto} that has been saved with ID and version.
   */
  ClientEto saveClient(ClientEto client);

}
