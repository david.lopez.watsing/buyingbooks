package com.devonfw.application.bb.authormanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.authormanagement.logic.api.Authormanagement;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorEto;
import com.devonfw.application.bb.authormanagement.logic.api.to.AuthorSearchCriteriaTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Authormanagement}.
 */
@Path("/authormanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface AuthormanagementRestService {

  /**
   * Delegates to {@link Authormanagement#findAuthor}.
   *
   * @param id the ID of the {@link AuthorEto}
   * @return the {@link AuthorEto}
   */
  @GET
  @Path("/author/{id}/")
  public AuthorEto getAuthor(@PathParam("id") long id);

  /**
   * Delegates to {@link Authormanagement#saveAuthor}.
   *
   * @param author the {@link AuthorEto} to be saved
   * @return the recently created {@link AuthorEto}
   */
  @POST
  @Path("/author/")
  public AuthorEto saveAuthor(AuthorEto author);

  /**
   * Delegates to {@link Authormanagement#deleteAuthor}.
   *
   * @param id ID of the {@link AuthorEto} to be deleted
   */
  @DELETE
  @Path("/author/{id}/")
  public void deleteAuthor(@PathParam("id") long id);

  /**
   * Delegates to {@link Authormanagement#findAuthorEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding authors.
   * @return the {@link Page list} of matching {@link AuthorEto}s.
   */
  @Path("/author/search")
  @POST
  public Page<AuthorEto> findAuthors(AuthorSearchCriteriaTo searchCriteriaTo);

}