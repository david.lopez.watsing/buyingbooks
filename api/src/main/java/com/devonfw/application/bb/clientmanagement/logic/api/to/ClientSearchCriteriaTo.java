package com.devonfw.application.bb.clientmanagement.logic.api.to;

import com.devonfw.application.bb.general.common.api.to.AbstractSearchCriteriaTo;
import com.devonfw.module.basic.common.api.query.StringSearchConfigTo;

/**
 * {@link SearchCriteriaTo} to find instances of {@link com.devonfw.application.bb.clientmanagement.common.api.Client}s.
 */
public class ClientSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String name;

  private String lastname;

  private StringSearchConfigTo nameOption;

  private StringSearchConfigTo lastnameOption;

  /**
   * @return nameId
   */

  public String getName() {

    return name;
  }

  /**
   * @param name setter for name attribute
   */

  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return lastnameId
   */

  public String getLastname() {

    return lastname;
  }

  /**
   * @param lastname setter for lastname attribute
   */

  public void setLastname(String lastname) {

    this.lastname = lastname;
  }

  /**
   * @return the {@link StringSearchConfigTo} used to search for {@link #getName() name}.
   */
  public StringSearchConfigTo getNameOption() {

    return this.nameOption;
  }

  /**
   * @param nameOption new value of {@link #getNameOption()}.
   */
  public void setNameOption(StringSearchConfigTo nameOption) {

    this.nameOption = nameOption;
  }

  /**
   * @return the {@link StringSearchConfigTo} used to search for {@link #getLastname() lastname}.
   */
  public StringSearchConfigTo getLastnameOption() {

    return this.lastnameOption;
  }

  /**
   * @param lastnameOption new value of {@link #getLastnameOption()}.
   */
  public void setLastnameOption(StringSearchConfigTo lastnameOption) {

    this.lastnameOption = lastnameOption;
  }

}
