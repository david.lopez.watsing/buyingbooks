package com.devonfw.application.bb.clientmanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.devonfw.application.bb.clientmanagement.logic.api.Clientmanagement;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientEto;
import com.devonfw.application.bb.clientmanagement.logic.api.to.ClientSearchCriteriaTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Clientmanagement}.
 */
@Path("/clientmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ClientmanagementRestService {

  /**
   * Delegates to {@link Clientmanagement#findClient}.
   *
   * @param id the ID of the {@link ClientEto}
   * @return the {@link ClientEto}
   */
  @GET
  @Path("/client/{id}/")
  public ClientEto getClient(@PathParam("id") long id);

  /**
   * Delegates to {@link Clientmanagement#saveClient}.
   *
   * @param client the {@link ClientEto} to be saved
   * @return the recently created {@link ClientEto}
   */
  @POST
  @Path("/client/")
  public ClientEto saveClient(ClientEto client);

  /**
   * Delegates to {@link Clientmanagement#deleteClient}.
   *
   * @param id ID of the {@link ClientEto} to be deleted
   */
  @DELETE
  @Path("/client/{id}/")
  public void deleteClient(@PathParam("id") long id);

  /**
   * Delegates to {@link Clientmanagement#findClientEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding clients.
   * @return the {@link Page list} of matching {@link ClientEto}s.
   */
  @Path("/client/search")
  @POST
  public Page<ClientEto> findClients(ClientSearchCriteriaTo searchCriteriaTo);

}