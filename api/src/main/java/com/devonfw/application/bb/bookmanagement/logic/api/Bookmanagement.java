package com.devonfw.application.bb.bookmanagement.logic.api;

import com.devonfw.application.bb.bookmanagement.logic.api.usecase.UcFindBook;
import com.devonfw.application.bb.bookmanagement.logic.api.usecase.UcManageBook;

/**
 * Interface for Bookmanagement component.
 */
public interface Bookmanagement extends UcFindBook, UcManageBook {

}
