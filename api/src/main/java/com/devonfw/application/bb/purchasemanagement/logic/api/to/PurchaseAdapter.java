package com.devonfw.application.bb.purchasemanagement.logic.api.to;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author dlopezwa
 *
 */
public class PurchaseAdapter {

  /**
   * Parses the.
   *
   * @param purchaseInCto the purchase in cto
   * @return the purchase eto
   */
  public static PurchaseEto parseToPurchaseEto(PurchaseInCto purchaseInCto) {

    PurchaseEto purchaseEto = new PurchaseEto();
    purchaseEto.setBookId(purchaseInCto.getBookId());
    purchaseEto.setClientId(purchaseInCto.getClientId());
    purchaseEto.setPurchaseDate(new Timestamp((new Date()).getTime()));

    return purchaseEto;
  }

}
