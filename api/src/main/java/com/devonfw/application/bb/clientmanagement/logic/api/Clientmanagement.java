package com.devonfw.application.bb.clientmanagement.logic.api;

import com.devonfw.application.bb.clientmanagement.logic.api.usecase.UcFindClient;
import com.devonfw.application.bb.clientmanagement.logic.api.usecase.UcManageClient;

/**
 * Interface for Clientmanagement component.
 */
public interface Clientmanagement extends UcFindClient, UcManageClient {

}
